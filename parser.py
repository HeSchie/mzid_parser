#!/usr/bin/env python3
#-*- conding:utf-8 -*-

import xml.etree.ElementTree as ET
import pandas as pd
import os

# To keep the consistency with .mzid file,
# cammel case and snake case are mixed in .mzid file.
# For example, dBSequence_ref is an attribute of <DBSequence> from .mzid file
# I always try to use snake case, If the functions or variables are defined by me.

def db_sequence_df_def(xml_root, name_space):
    '''Fetches protein (dBSequence_ref) information from mzid file
    accession and id are included in tag <DBSequence>

    Args:
        xml_root: an xml root object
        name_space: xml files may indicate name space
    Return:
        a pandas data frame includes DBSequence informaction.
        DF fields: dBSequence_ref and accession
    '''
    DBSequence_list = []
    for DBSequence in xml_root.iter(name_space + 'DBSequence'):
        DBSequence_item = {}
        DBSequence_item['dBSequence_ref'] = DBSequence.get('id')
        DBSequence_item['accession'] = DBSequence.get('accession')
        DBSequence_list = DBSequence_list + [DBSequence_item]
    return pd.DataFrame(DBSequence_list)

def proteinAmbiguityGroup_df_def(xml_root, name_space):
    '''Fetches protein group with spectrum information from <ProteinAmbiguityGroup>

    Args:
        xml_root: an xml root object
        name_space: xml files may indicate name space
    Return:
        a pandas data frame includes protein group and spectrum information
        DF fields - protein_group, dBSequence_ref and spectrum_identification
    '''
    proteinAmbiguityGroup_list = []
    for proteinAmbiguityGroup in xml_root.iter(name_space + 'ProteinAmbiguityGroup'):
        proteinDetectionHypothesisIter = proteinAmbiguityGroup.iter(name_space+'ProteinDetectionHypothesis')
        for protein_detection_hypothesis in proteinDetectionHypothesisIter:
            dBSequence = protein_detection_hypothesis.attrib['dBSequence_ref']
            for spectrum_identification_item in protein_detection_hypothesis.iter(name_space+'SpectrumIdentificationItemRef'):
                proteinAmbiguityGroup_dict = {}
                proteinAmbiguityGroup_dict['protein_group'] = proteinAmbiguityGroup.attrib['id']#.split('_')[1]
                proteinAmbiguityGroup_dict['dBSequence_ref'] = dBSequence
                proteinAmbiguityGroup_dict['spectrum_identification'] = spectrum_identification_item.attrib['spectrumIdentificationItem_ref']
                proteinAmbiguityGroup_list = proteinAmbiguityGroup_list + [proteinAmbiguityGroup_dict]
    return pd.DataFrame(proteinAmbiguityGroup_list)

def sample_spectrum_df_def(xml_root, name_space):
    '''Fetches spectrum and its coresponding sample info
    In some cases, <AnalysisSampleCollection> tag exist, while it is
    not indicated in most of the time. But it is still possible to get
    the information form <SpectrumIdentificationResult>, spectraData_ref
    is the sample id.

    Args:
        xml_root: an xml root object
        name_space: xml files may indicate name space
    Return:
        a pandas data frame includes sample and stpectrum
        DF fields: sample, spectrum_identification
    '''
    sample_spectrum_list = []
    for spectrumIdentification in xml_root.iter(name_space + 'SpectrumIdentificationResult'):
        sample_spectrum = {}
        sample_spectrum['sample'] = spectrumIdentification.get('spectraData_ref')
        spectrumIdentification_item = spectrumIdentification.find(name_space + 'SpectrumIdentificationItem')
        sample_spectrum['spectrum_identification'] = spectrumIdentification_item.get('id')
        sample_spectrum_list = sample_spectrum_list + [sample_spectrum]
    return pd.DataFrame(sample_spectrum_list)


def read_mizd_def(mzid_file):
    '''Reads mzid file
    Args:
        mzid_file: a mzid file
    Return:
        an xml root object read from mzid file.
    Raises:
        raises errors when file not found or no group info
    '''
    try:
        # read mzid file
        tree = ET.parse(mzid_file)
        root = tree.getroot()
    except FileNotFoundError:
        raise RuntimeError(mzid_file + ' can not be found!')

    # namespace
    if root.tag[0] == '{':
        name_space = '{' + root.tag.split('}')[0].strip('{}') + '}'
    else:
        name_space = ''

    # check the existence of protein group information
    a = root.iter(name_space + 'Role')
    if not any(True for _ in root.iter(name_space + 'ProteinDetectionList')):
        # return 'There is no protein group information.'
        raise RuntimeError(mzid_file + ' includes no protein group information!')

    return root, name_space

def read_mizd2df_def(mzid_file):
    '''Fetch protein accessions (one group) and its spectrum count
    Args:
        mzid_file: a string to indicate mzid file
    Return:
        a data frame includes protein accessions (one group) and its spectrum count
    '''
    root, name_space = read_mizd_def(mzid_file)

    # protein
    dBSequence_ref_accession_df = db_sequence_df_def(root, name_space)

    # spectrum and protein group
    group_dBSequence_spectrum_df = proteinAmbiguityGroup_df_def(root, name_space)

    # sample and spectrum_identification
    sample_spectrum_df = sample_spectrum_df_def(root, name_space)

    # (sample, spectrum_identification) inner_join (protein_group, dBSequence_ref, spectrum_identification) on `spectrum_identification`
    sample_spectrum_group_dBSequence_df = pd.merge(sample_spectrum_df, \
                                                    group_dBSequence_spectrum_df, \
                                                    on='spectrum_identification', \
                                                    how='inner')

    # (dBSequence_ref, accession) inner_join (protein_group, dBSequence_ref, spectrum_identification) on `dBSequence_ref`
    sample_spectrum_group_peptide_accession_df = pd.merge(sample_spectrum_group_dBSequence_df, \
                                                            dBSequence_ref_accession_df, \
                                                            on='dBSequence_ref', \
                                                            how='inner')

    # group_by `protein_group`, count(`spectrum_identification`)
    group_sample_count_df = sample_spectrum_group_peptide_accession_df \
                            .groupby(['protein_group', 'sample'], \
                            as_index=False) \
                            .agg({'spectrum_identification': pd.Series.nunique})

    # group_by `protein_group`, merge all proteins (name) into a string under one group
    group_accessions_df = sample_spectrum_group_peptide_accession_df \
                            .groupby('protein_group', as_index=False) \
                            .agg({'accession': lambda x: x.unique().tolist()})

    # (protein_group, accession) inner_jon (protein_group, spectrum_identification) on `protein_group`
    return_data = pd.merge(group_sample_count_df, group_accessions_df, on='protein_group', how='inner')

    # rename and drop unnecessary columns
    return_data.rename(columns={'spectrum_identification':'quant', 'accession': 'proteins'}, inplace=True)
    return_data = return_data.drop(['protein_group'], axis=1)
    return return_data


mzid_file = './data/Yeast_1111_Ac-Cys_plus3.mzid'   # without <AnalysisSampleCollection>
# mzid_file = './data/MSB29562CplusBand_10 (F127143).mzid' # with <AnalysisSampleCollection>
result = read_mizd2df_def(mzid_file)
print(result[1:20])